package com.android.appium.api.demos.tests.views.buttons;

import com.android.appium.api.demos.tests.BaseTest;
import com.android.appium.api.demos.views.api_demos_home.ApiDemosHomeView;
import com.android.appium.api.demos.views.views.ViewsView;
import com.android.appium.api.demos.views.views.buttons.ButtonsView;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class ButtonsTest  extends BaseTest{
    ApiDemosHomeView apiDemosHomeView;
    ViewsView viewsView;
    ButtonsView buttonsView;
    @BeforeClass
    public void navigateToButtonsView(){
        apiDemosHomeView = new ApiDemosHomeView(driver);
        viewsView = new ViewsView(driver);
        buttonsView = new ButtonsView(driver);
        apiDemosHomeView.clickOnViewsButton();
        viewsView.clickOnButtonsButton();
    }
    /**
     * Test case to verify buttons.
     */
    @Test(description = "Test ase to verify buttons.")
    public void verifyButtons(){
        boolean isDisplayedNormalButton = buttonsView.isNormalButtonDisplayed();
        Assert.assertTrue("Normal button is not displayed",isDisplayedNormalButton);

        boolean isDisplayedSmallButton = buttonsView.isSmallButtonDisplayed();
        Assert.assertTrue("Small button is not displayed",isDisplayedSmallButton);

        buttonsView.clickOnOffButton();
        boolean isDisplayedOnButton = buttonsView.isOnButtonDisplayed();
        Assert.assertTrue("ON button is not displayed",isDisplayedOnButton);

        buttonsView.clickOnOnButton();
        boolean isDisplayedOffButton = buttonsView.isOffButtonDisplayed();
        Assert.assertTrue("OFF button is not displayed",isDisplayedOffButton);
    }
}
