package com.android.appium.api.demos.tests;

import com.android.appium.api.demos.init.CapabilityBuilder;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.net.URL;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class BaseTest {
    CapabilityBuilder capabilityBuilder = new CapabilityBuilder();
    public static AndroidDriver<MobileElement> driver;

    @BeforeTest
    public void launchEmulator() throws Exception{
        File file = new File("src\\main\\resources\\ApiDemos-debug.apk");
        DesiredCapabilities capabilities = capabilityBuilder.getCapabilities();
        capabilities.setCapability("app", file.getAbsolutePath());
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
    }

    @AfterClass

    public void tearDown(){
        driver.quit();
    }
}
