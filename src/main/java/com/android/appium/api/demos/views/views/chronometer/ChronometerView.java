package com.android.appium.api.demos.views.views.chronometer;

import com.android.appium.api.demos.views.BaseView;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class ChronometerView extends BaseView {
    public ChronometerView(AndroidDriver<MobileElement> driver){
        super(driver);
       // PageFactory.initElements(new AppiumFieldDecorator(driver),this);
    }
    /**
     * Method to check whether Initial format Displayed or not.
     * @return boolean
     */
    public boolean isDisplayedInitialFormat(){
        WebElement initialFormat = driver.findElement(By.name("contains(@text,'Initial format')"));
        boolean isDisplayedInitialFormat = initialFormat.isDisplayed();
        return isDisplayedInitialFormat;
    }

    /**
     * Method to click on Start button.
     */
    public void clickOnStartButton(){
        driver.findElement(By.name("Start")).click();
    }

    /**
     * Method to click on Stop button.
     */
    public void clickOnStopButton(){
        driver.findElement(By.name("Stop")).click();
    }

    /**
     * Method to click on Reset button.
     */
    public void clickOnResetButton(){
        driver.findElement(By.name("Reset")).click();
    }

    /**
     * Method to check whether Set format string displayed or not.
     * @return
     */
    public boolean isDisplayedSetFormatString(){
        WebElement setFormatString = driver.findElement(By.name("Set format string"));
        boolean isDisplayed = setFormatString.isDisplayed();
        return isDisplayed;
    }

    /**
     * Method to check whether Clear format string displayed or not.
     * @return
     */
    public boolean isDisplayedClearFormatString(){
        WebElement clearFormatString = driver.findElement(By.name("Clear format string"));
        boolean isDisplayed = clearFormatString.isDisplayed();
        return isDisplayed;

    }
}
