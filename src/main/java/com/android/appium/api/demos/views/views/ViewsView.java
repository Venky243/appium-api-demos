package com.android.appium.api.demos.views.views;

import com.android.appium.api.demos.views.BaseView;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class ViewsView extends BaseView {
        public ViewsView(AndroidDriver<MobileElement> driver){
            super(driver);
            //PageFactory.initElements(new AppiumFieldDecorator(driver),this);
        }
    /**
     * Method to click on Views button.
     */
    public void clickOnButtonsButton(){
        MobileElement buttons = driver.findElement(By.name("Buttons"));
        buttons.click();
    }

    /**
     * Method to click on Chronometer button.
     */
    public void clickOnChronometer(){
        MobileElement chronometer = driver.findElement(By.name("Chronometer"));
        chronometer.click();
    }
}
