package com.android.appium.api.demos.views;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class BaseView {
    public AndroidDriver<MobileElement> driver;

    public BaseView(AndroidDriver<MobileElement> driver){
        this.driver = driver;
    }


    public void scrollPageDown(int duration) {

        Dimension size = driver.manage().window().getSize();
        System.out.println(size);
        int starty = (int) (size.height * 0.80);
        int endy = (int) (size.height * 0.20);
        int startx = size.width / 2;
        System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);
        TouchAction action = new TouchAction(driver);
        try {
            action.press(startx,starty).waitAction(duration).moveTo(startx,endy).release().perform();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void scrollPageUp(int duration){
        Dimension size = driver.manage().window().getSize();
        System.out.println(size);
        int starty = (int) (size.height * 0.80);
        int endy = (int) (size.height * 0.20);
        int startx = size.width / 2;
        System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);
        TouchAction action = new TouchAction(driver);
        try {
            action.press(startx, endy).waitAction(duration).moveTo(startx, starty).release().perform();
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
