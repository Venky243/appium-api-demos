package com.android.appium.api.demos.views.views.buttons;

import com.android.appium.api.demos.views.BaseView;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class ButtonsView extends BaseView {
        public ButtonsView(AndroidDriver<MobileElement> driver){
            super(driver);
            //PageFactory.initElements(new AppiumFieldDecorator(driver),this);
        }
    /**
     * Method to click on Off button.
     */
    public void clickOnOffButton(){
        MobileElement off = driver.findElement(By.name("OFF"));
        off.click();
    }

    /**
     *Method to click on On button.
     */
    public void clickOnOnButton(){
        MobileElement on = driver.findElement(By.name("ON"));
        on.click();
    }

    /**
     * Method to check whether Normal button displayed or not.
     * @return boolean
     */
    public boolean isNormalButtonDisplayed(){
        WebElement normalButton =  driver.findElement(By.name("Normal"));
        boolean isDisplayed = normalButton.isDisplayed();
        return isDisplayed;
    }

    /**
     * Method to check whether Small button is displayed or not.
     * @return boolean
     */
    public boolean isSmallButtonDisplayed(){
        WebElement smallButton = driver.findElement(By.name("Small"));
        boolean isDisplayed = smallButton.isDisplayed();
        return isDisplayed;
    }

    /**
     * Method to check whether Off button is displayed or not.
     * @return boolean
     */
    public boolean isOffButtonDisplayed(){
        WebElement offButton = driver.findElement(By.name("OFF"));
        boolean isDisplayed = offButton.isDisplayed();
        return isDisplayed;
    }

    /**
     * Method to check whether On button is displayed or not.
     * @return
     */
    public boolean isOnButtonDisplayed(){
        WebElement onButton = driver.findElement(By.name("ON"));
        boolean isDisplayedOnButton = onButton.isDisplayed();
        return isDisplayedOnButton;
    }


}
