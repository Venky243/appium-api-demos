package com.android.appium.api.demos.init;

import com.android.appium.api.demos.util.PropertyUtil;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class CapabilityBuilder {
    PropertyUtil propertyUtil = new PropertyUtil();

    public DesiredCapabilities getCapabilities(){
        String deviceName = propertyUtil.getProperty("deviceName");
        String platformName = propertyUtil.getProperty("Android");
        String platformVersion = propertyUtil.getProperty("platformVersion");
        String appPackage = propertyUtil.getProperty("appPackage");
        String appActivity = propertyUtil.getProperty("appActivity");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,deviceName);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,platformName);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,platformVersion);
        capabilities.setCapability("appPackage",appPackage);
        capabilities.setCapability("appActivity",appActivity);
        return capabilities;
    }
}
