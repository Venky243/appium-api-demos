package com.android.appium.api.demos.util;

import java.util.Properties;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class PropertyUtil {
    /**
     * Instance variable for properties.
     */
    Properties properties = new Properties();

    /**
     * Constructor to load the all property files.
     */
    public PropertyUtil() {
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
        } catch (final Exception e) {
            e.printStackTrace();
        }
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream(""));
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to get the property value.
     * @param property
     * @return
     */
    public String getProperty(final String property) {
        return properties.getProperty(property);
    }

}
