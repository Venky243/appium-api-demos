package com.android.appium.api.demos.views.api_demos_home;

import com.android.appium.api.demos.views.BaseView;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

/**
 * Created by venkatesh on 2/27/2017.
 */
public class ApiDemosHomeView extends BaseView {

    public ApiDemosHomeView(AndroidDriver<MobileElement> driver){
        super(driver);
        //PageFactory.initElements(new AppiumFieldDecorator(driver),ApiDemosHomeView.class);
    }
    /**
     * Method to click on Views button.
     */
    public void clickOnViewsButton(){
        scrollPageDown(1500);
        MobileElement views = driver.findElement(By.name("Views"));
        views.click();
    }
}
